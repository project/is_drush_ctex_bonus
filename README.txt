About
-----

  This little module checks to see if a module being enabled includes Ctools Export Bonus exportables. If it does, it notifies the user and offers suggestions for importing or rebuilding exportables (and information for installing Drush and Ctools Export Bonus if they're not already installed).


Usage
-----

  Enable this module. It just works.


Installation
------------

  1. Place is_drush_ctex_bonus in one of these directories: 

    - sited/all/modules

    - sites/default/modules

    - sites/$site/modules

    - profiles/$profile/modules.


  2. Go to Administration -> Modules.

  3. Enable the Is Ctools Export Bonus module.


Maintainer
----------

  Bryan Hirsch, bryan@bryanhirsch.com
